<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('login/facebook', 'Auth\LoginController@redirectToProvider');

Route::get('login/facebook/callback', 'Auth\LoginController@handleProviderCallback');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/data', 'FacebookController@data');

Route::get('posts/{post}', 'FacebookController@posts');

Route::get('photoupload', 'FacebookController@photoupload');

Route::post('photoupload', 'FacebookController@photoupload');

Route::post('videoupload', 'FacebookController@videoupload');