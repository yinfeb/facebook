<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Socialite;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')
        ->scopes(['public_profile', 'email', 'read_insights', 'read_audience_network_insights', 'read_page_mailboxes', 'manage_pages', 'publish_pages', 'publish_actions', 'rsvp_event', 'pages_show_list', 'pages_manage_cta', 'pages_manage_instant_articles'])
        ->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback(Request $request)
    {
        $user = Socialite::driver('facebook')->user();
        $code = $request->input('code'); 
        $accessToken = $user->token;
        $fb = new \Facebook\Facebook([
            'app_id' => '996058567192954',
            'app_secret' => '268777104e0fa6bb674e8aa9ad1bb104',
            'default_graph_version' => 'v2.8',
        ]);

        try {
            $response = $fb->get('/me/accounts', $user->token);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }

        $accounts = $response->getGraphEdge();

        try {
            // Returns a `Facebook\FacebookResponse` object
            $response = $fb->get('/me?fields=id,name,email,picture', $accessToken);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
          echo 'Graph returned an error: ' . $e->getMessage();
          exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          echo 'Facebook SDK returned an error: ' . $e->getMessage();
          exit;
        }

        $GraphUser = $response->getGraphUser();
        $page_title = "Editors";
        $request->session()->push('user',$GraphUser);
        $request->session()->put('accessToken',$accessToken);
        return view('editors', compact('page_title','GraphUser','accounts','code','accessToken'));
    }
}
