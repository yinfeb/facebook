<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class FacebookController extends Controller
{
  public function photoupload(Request $request)
  {
    $post = $request->input('editor1');
    $accessToken = $request->session()->get('accessToken');
    $code = $request->input('code');
    $plaintext = strip_tags($post);

    $fb = new \Facebook\Facebook([
      'app_id' => '996058567192954',
      'app_secret' => '268777104e0fa6bb674e8aa9ad1bb104',
      'default_graph_version' => 'v2.8',
    ]);

    try {
        $response = $fb->get('/me/accounts', $accessToken);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }

    $accounts = $response->getGraphEdge();
    $id = $accounts['0']['id'];
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if (is_uploaded_file($_FILES["image"]["tmp_name"])) { 
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $mime = finfo_file($finfo, $_FILES["image"]["tmp_name"]);
            $allowed = array("image/gif", "image/jpg", "image/jpeg", "image/png");
            $image = $_FILES["image"]["tmp_name"];
            // upload image
            if (in_array($mime, $allowed)) { 
                $data = [
                  'message' => $plaintext,
                  'source' => $fb->fileToUpload($image),
                ];         
                $fbApp = new \Facebook\FacebookApp('996058567192954', '268777104e0fa6bb674e8aa9ad1bb104');
                $request = new \Facebook\FacebookRequest($fbApp, $accounts['0']['access_token'], 'POST', "/$id/photos", $data);

                // Send the request to Graph
                try {
                $response = $fb->getClient()->sendRequest($request);
                } catch(Facebook\Exceptions\FacebookResponseException $e) {
                // When Graph returns an error
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
                } catch(Facebook\Exceptions\FacebookSDKException $e) {
                // When validation fails or other local issues
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
                }
                $graphNode = $response->getGraphNode();
                echo 'Photo ID: ' . $graphNode['id'];
                $providerUser = $response->getGraphUser();
            }
        }
        else {
          // update status message
          $data = [
            'message' => $plaintext,
          ];
          $fbApp = new \Facebook\FacebookApp('996058567192954', '268777104e0fa6bb674e8aa9ad1bb104');
          $request = new \Facebook\FacebookRequest($fbApp, $accounts['0']['access_token'], 'POST', "/$id/feed", $data);
          // Send the request to Graph
          try {
            $response = $fb->getClient()->sendRequest($request);
          } catch(Facebook\Exceptions\FacebookResponseException $e) {
          // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
          } catch(Facebook\Exceptions\FacebookSDKException $e) {
          // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
          }
          $graphNode = $response->getGraphNode();
        }
      }

      try {
        // Returns a `Facebook\FacebookResponse` object
        $response = $fb->get('/me?fields=id,name,email,picture', $accessToken);
      } catch(Facebook\Exceptions\FacebookResponseException $e) {
          echo 'Graph returned an error: ' . $e->getMessage();
          exit;
      } catch(Facebook\Exceptions\FacebookSDKException $e) {
          echo 'Facebook SDK returned an error: ' . $e->getMessage();
          exit;
      }
      $GraphUser = $response->getGraphUser();
      $page_title = "Editors";
      return view('editors', compact('page_title','GraphUser','accounts','accessToken','code'));
  }

    public function videoupload(Request $request)
  {
    $post = $request->input('editor2');
    $accessToken = $request->input('accessToken');
    $code = $request->input('code');
    $plaintext = strip_tags($post);

    $fb = new \Facebook\Facebook([
      'app_id' => '996058567192954',
      'app_secret' => '268777104e0fa6bb674e8aa9ad1bb104',
      'default_graph_version' => 'v2.8',
    ]);

    try {
        $response = $fb->get('/me/accounts', $accessToken);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }

    $accounts = $response->getGraphEdge();

    $id = $accounts['0']['id'];

    if (is_uploaded_file($_FILES["video"]["tmp_name"])) {
        $video = $_FILES["video"]["tmp_name"];
        $data = [
          'title' => 'My Foo Video',
          'message' => $plaintext,
          'source' => $fb->videoToUpload($video),
        ];
        $fbApp = new \Facebook\FacebookApp('996058567192954', '268777104e0fa6bb674e8aa9ad1bb104');
        $request = new \Facebook\FacebookRequest($fbApp, $accounts['0']['access_token'], 'POST', "/$id/videos", $data);

        // Send the request to Graph
        try {
          $response = $fb->getClient()->sendRequest($request);
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
          // When Graph returns an error
          echo 'Graph returned an error: ' . $e->getMessage();
          exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          // When validation fails or other local issues
          echo 'Facebook SDK returned an error: ' . $e->getMessage();
          exit;
        }
        $graphNode = $response->getGraphNode();
        echo 'Video ID: ' . $graphNode['id'];
        }
    else {
      // update status message
      $data = [
        'message' => $plaintext,
      ];
      $fbApp = new \Facebook\FacebookApp('996058567192954', '268777104e0fa6bb674e8aa9ad1bb104');
      $request = new \Facebook\FacebookRequest($fbApp, $accounts['0']['access_token'], 'POST', "/$id/feed", $data);
      // Send the request to Graph
      try {
        $response = $fb->getClient()->sendRequest($request);
      } catch(Facebook\Exceptions\FacebookResponseException $e) {
      // When Graph returns an error
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
      } catch(Facebook\Exceptions\FacebookSDKException $e) {
      // When validation fails or other local issues
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
      }
      $graphNode = $response->getGraphNode();
    }

      try {
        // Returns a `Facebook\FacebookResponse` object
        $response = $fb->get('/me?fields=id,name,email,picture', $accessToken);
      } catch(Facebook\Exceptions\FacebookResponseException $e) {
          echo 'Graph returned an error: ' . $e->getMessage();
          exit;
      } catch(Facebook\Exceptions\FacebookSDKException $e) {
          echo 'Facebook SDK returned an error: ' . $e->getMessage();
          exit;
      }
      $GraphUser = $response->getGraphUser();

      return view('editors', compact('page_title','GraphUser','accounts','accessToken','code'));
  }  


    public function data(Request $request)
  {
    $fb = new \Facebook\Facebook([
      'app_id' => '996058567192954',
      'app_secret' => '268777104e0fa6bb674e8aa9ad1bb104',
      'default_graph_version' => 'v2.8',
    ]);
    $accessToken = $request->session()->get('accessToken');
    $code = $request->session()->get('code');
    try {
        $response = $fb->get('/me/accounts', $request->session()->get('accessToken'));
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }

    $accounts = $response->getGraphEdge();
    $id = $accounts['0']['id'];
    $fbApp = new \Facebook\FacebookApp('996058567192954', '268777104e0fa6bb674e8aa9ad1bb104');
    $request = new \Facebook\FacebookRequest($fbApp, $accounts['0']['access_token'], 'GET', "/$id/posts?fields=message,created_time");

    // Send the request to Graph
    try {
      $response = $fb->getClient()->sendRequest($request);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      // When Graph returns an error
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      // When validation fails or other local issues
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }
    $graphEdge = $response->getGraphEdge();

    try {
      // Returns a `Facebook\FacebookResponse` object
      $response = $fb->get('/me?fields=id,name,email,picture', $accessToken);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    $GraphUser = $response->getGraphUser();

    $page_title = "Page Posts";

    return view('data', compact('page_title','GraphUser','accounts','accessToken','code','graphEdge'));
  }


    public function posts(Request $request,$id)
  {
    $fb = new \Facebook\Facebook([
      'app_id' => '996058567192954',
      'app_secret' => '268777104e0fa6bb674e8aa9ad1bb104',
      'default_graph_version' => 'v2.8',
    ]);
    $accessToken = $request->session()->get('accessToken');
    $code = $request->session()->get('code');
    try {
        $response = $fb->get('/me/accounts', $request->session()->get('accessToken'));
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
        echo 'Graph returned an error: ' . $e->getMessage();
        exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
        echo 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
    }

    $accounts = $response->getGraphEdge();
      
    $fbApp = new \Facebook\FacebookApp('996058567192954', '268777104e0fa6bb674e8aa9ad1bb104');
    $request = new \Facebook\FacebookRequest($fbApp, $accounts['0']['access_token'], 'GET', "$id?fields=message,created_time,shares,likes.summary(true),comments.summary(true)");

    // Send the request to Graph
    try {
      $response = $fb->getClient()->sendRequest($request);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      // When Graph returns an error
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      // When validation fails or other local issues
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    $postNode = $response->getGraphNode();

    // Send the request to Graph
    try {
      $response = $fb->get("$id/insights/post_impressions", $accounts['0']['access_token']);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      // When Graph returns an error
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      // When validation fails or other local issues
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    $post_impressions = $response->getGraphEdge();

    try {
      $response = $fb->get("$id/insights/post_impressions_unique", $accounts['0']['access_token']);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      // When Graph returns an error
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      // When validation fails or other local issues
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    $post_impressions_unique = $response->getGraphEdge();

    try {
      $response = $fb->get("$id/insights/post_engaged_users", $accounts['0']['access_token']);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      // When Graph returns an error
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      // When validation fails or other local issues
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    $post_engaged_users = $response->getGraphEdge();

    try {
      $response = $fb->get("$id/insights/post_negative_feedback", $accounts['0']['access_token']);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      // When Graph returns an error
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      // When validation fails or other local issues
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    $post_negative_feedback = $response->getGraphEdge();

    try {
      $response = $fb->get("$id/insights/post_negative_feedback_unique", $accounts['0']['access_token']);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      // When Graph returns an error
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      // When validation fails or other local issues
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    $post_negative_feedback_unique = $response->getGraphEdge();

    try {
      $response = $fb->get("$id/insights/post_impressions_paid", $accounts['0']['access_token']);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      // When Graph returns an error
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      // When validation fails or other local issues
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    $post_impressions_paid = $response->getGraphEdge();

    try {
      $response = $fb->get("$id/insights/post_impressions_paid_unique", $accounts['0']['access_token']);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      // When Graph returns an error
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      // When validation fails or other local issues
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    $post_impressions_paid_unique = $response->getGraphEdge();

    try {
      $response = $fb->get("$id/insights/post_impressions_fan", $accounts['0']['access_token']);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      // When Graph returns an error
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      // When validation fails or other local issues
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    $post_impressions_fan = $response->getGraphEdge();

    try {
      $response = $fb->get("$id/insights/post_impressions_fan_unique", $accounts['0']['access_token']);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      // When Graph returns an error
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      // When validation fails or other local issues
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    $post_impressions_fan_unique = $response->getGraphEdge();

    try {
      // Returns a `Facebook\FacebookResponse` object
      $response = $fb->get('/me?fields=id,name,email,picture', $accessToken);
    } catch(Facebook\Exceptions\FacebookResponseException $e) {
      echo 'Graph returned an error: ' . $e->getMessage();
      exit;
    } catch(Facebook\Exceptions\FacebookSDKException $e) {
      echo 'Facebook SDK returned an error: ' . $e->getMessage();
      exit;
    }

    $GraphUser = $response->getGraphUser();

    $page_title = "Page Post Insights";

    return view('post_impressions', compact('page_title','GraphUser','accounts','accessToken','code','postNode','post_impressions','post_impressions_unique','post_engaged_users','post_negative_feedback','post_negative_feedback_unique','post_impressions_paid','post_impressions_paid_unique','post_impressions_fan','post_impressions_fan_unique'));
  }
}