@extends('layouts.app')

@section('content')
    <!-- Main content -->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
                <ul>
                    <li>
                      <?php
                        $id = $graphEdge[0]['id'];
                        if(!isset($graphEdge[0]['message']))
                          echo "<a href=posts/$id>Post share without message</a>";
                        else
                        {
                          $message = $graphEdge[0]['message'];
                          echo "<a href=posts/$id>$message</a>";
                        }
                      ?>
                    </li>
                    <li>
                      <?php
                        $id = $graphEdge[1]['id'];
                        if(!isset($graphEdge[1]['message']))
                          echo "<a href=posts/$id>Post share without message</a>";
                        else
                        {
                          $message = $graphEdge[1]['message'];
                          echo "<a href=posts/$id>$message</a>";
                        }
                      ?>
                    </li>
                    <li>
                      <?php
                        $id = $graphEdge[2]['id'];
                        if(!isset($graphEdge[2]['message']))
                          echo "<a href=posts/$id>Post share without message</a>";
                        else
                        {
                          $message = $graphEdge[2]['message'];
                          echo "<a href=posts/$id>$message</a>";
                        }
                      ?>
                    </li>
                    <li>
                      <?php
                        $id = $graphEdge[3]['id'];
                        if(!isset($graphEdge[3]['message']))
                          echo "<a href=posts/$id>Post share without message</a>";
                        else
                        {
                          $message = $graphEdge[3]['message'];
                          echo "<a href=posts/$id>$message</a>";
                        }
                      ?>
                    </li>
                    <li>
                      <?php
                        $id = $graphEdge[4]['id'];
                        if(!isset($graphEdge[4]['message']))
                          echo "<a href=posts/$id>Post share without message</a>";
                        else
                        {
                          $message = $graphEdge[4]['message'];
                          echo "<a href=posts/$id>$message</a>";
                        }
                      ?>
                    </li>
                </ul>
            </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    <!-- /.content -->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
@endsection