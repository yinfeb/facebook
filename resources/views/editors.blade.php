@extends('layouts.app')

@section('content')
    <!-- Main content -->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Create a Post
              </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
              <form enctype="multipart/form-data" method="POST" action="{{ url('photoupload') }}">
                <textarea id="editor1" name="editor1" rows="10" cols="80"></textarea>
                <input type="file" name="image" id="image">
                <input type="hidden" name="accessToken" value="<?php echo $accessToken; ?>">
                <input type="hidden" name="code" value="<?php echo $code; ?>">
                <script>
                    CKEDITOR.replace( 'editor1' );
                    $(".textarea").wysihtml5();
                </script>
                {{ csrf_field() }}
                <input type="submit" value="Submit">
              </form>
            </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Upload a video
              </h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
              <form enctype="multipart/form-data" method="POST" action="videoupload">
                <textarea id="editor2" name="editor2" rows="10" cols="80"></textarea>
                <input type="file" name="video" id="video">
                <input type="hidden" name="accessToken" value="<?php echo $accessToken; ?>">
                <input type="hidden" name="code" value="<?php echo $code; ?>">
                <script>
                    CKEDITOR.replace( 'editor2' );
                    $(".textarea").wysihtml5();
                </script>
                {{ csrf_field() }}
                <input type="submit" value="Submit">
              </form>
            </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    <!-- /.content -->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
@endsection