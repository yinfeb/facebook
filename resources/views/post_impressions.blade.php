@extends('layouts.app')

@section('content')
    <!-- Main content -->
      <div class="row">
        <div class="col-md-12">
          <div class="box box-info">
            <div class="box-header">
            </div>
            <!-- /.box-header -->
            <div class="box-body pad">
              <table style="width: 100%;">
                <tr>
                  <td>The post ID</td>
                  <td>
                    <?php
                        $id = $postNode['id'];
                        echo "$id"
                    ?>
                  </td>
                </tr>
                <tr>
                  <td>The status message in the post</td>
                  <td>
                    <?php
                        if(!isset($postNode['message']))
                          echo "Post share without message";
                        else
                        {
                          $message = $postNode['message'];
                          echo "$message";
                        }
                    ?>
                  </td>
                </tr>
                <tr>
                  <td>The time the post was initially published</td>
                  <td>
                    <?php
                        echo $postNode['created_time']->format("Y-m-d");
                    ?>
                  </td>
                </tr>
                <tr>
                  <td>The shares count of this post</td>
                  <td>
                    <?php
                        if(!isset($postNode['share']))
                          echo "0";
                        else
                        {
                          $metaData = $postNode['share']->getMetaData();
                          echo $metaData['summary']['total_count'];
                        }
                    ?>
                  </td>
                </tr>
                <tr>
                  <td>The likes count of this post</td>
                  <td>
                    <?php
                        $metaData = $postNode['likes']->getMetaData();
                        echo $metaData['summary']['total_count'];
                      ?>
                  </td>
                </tr>
                <tr>
                  <td>The comments count of this post</td>
                  <td>
                    <?php
                        $metaData = $postNode['comments']->getMetaData();
                        echo $metaData['summary']['total_count'];
                      ?>
                  </td>
                </tr>
                <tr>
                  <td>The number of people who clicked anywhere in your posts</td>
                  <td>
                    <?php
                        echo $post_engaged_users[0]['values'][0]['value'];
                      ?>
                  </td>
                </tr>
                <tr>
                  <td>The number of times people took a negative action in your post (e.g. hid it)</td>
                  <td>
                    <?php
                        echo $post_negative_feedback[0]['values'][0]['value'];
                      ?>
                  </td>
                </tr>
                <tr>
                  <td>The number of people who took a negative action in your post (e.g., hid it)</td>
                  <td>
                    <?php
                        echo $post_negative_feedback_unique[0]['values'][0]['value'];
                      ?>
                  </td>
                </tr>
                <tr>
                  <td>The number of impressions for your Page post</td>
                  <td>
                    <?php
                        echo $post_impressions[0]['values'][0]['value'];
                      ?>
                  </td>
                </tr>
                <tr>
                  <td>The number of people who saw your Page post</td>
                  <td>
                    <?php
                        echo $post_impressions_unique[0]['values'][0]['value'];
                      ?>
                  </td>
                </tr>
                <tr>
                  <td>The number of impressions for your Page post in an Ad or Sponsored Story</td>
                  <td>
                    <?php
                        echo $post_impressions_paid[0]['values'][0]['value'];
                      ?>
                  </td>
                </tr>
                <tr>
                  <td>The number of people who saw your Page post in an Ad or Sponsored Story</td>
                  <td>
                    <?php
                        echo $post_impressions_paid_unique[0]['values'][0]['value'];;
                      ?>
                  </td>
                </tr>
                <tr>
                  <td>The number of impressions for your Page post by people who have liked your Page</td>
                  <td>
                    <?php
                        echo $post_impressions_fan[0]['values'][0]['value'];
                      ?>
                  </td>
                </tr>
                <tr>
                  <td>The number of people who have like your Page who saw your Page post</td>
                  <td>
                    <?php
                        echo $post_impressions_fan_unique[0]['values'][0]['value'];
                      ?>
                  </td>
                </tr>
            </table>
            </div>
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col-->
      </div>
      <!-- ./row -->
    <!-- /.content -->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
@endsection